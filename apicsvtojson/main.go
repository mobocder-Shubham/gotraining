package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	csv "path/csv"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Asset not Found\n"))
		return
	}
	//fmt.Println("Starting the application...")
	data, err := ioutil.ReadFile("data.json")
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	//w.WriteHeader(http.StatusOK)
	//fmt.Println("Contents of file:")

	w.Write([]byte(data))
}

func main() {
	csv.Csv()
	http.HandleFunc("/", rootHandler)
	err := http.ListenAndServe(":8090", nil)
	if err != nil {
		//panic(err)
		fmt.Println(err)
		os.Exit(1)
	}

}
