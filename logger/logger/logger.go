package loger

import (
	"fmt"
	"io"
	"log"
	"os"
)

//for checking the path is exist or not in user direactory
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err

}

//for creating the file
func CreateFile(path string) {

	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if isError(err) {
			return
		}
		defer file.Close()
	}

	log.Println("File Created Successfully", path)
}

//for edit your created file
func WriteFile(path string) {

	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(path, os.O_RDWR, 0777)
	if isError(err) {
		return
	}
	defer file.Close()

	// Write some text do from user
	fmt.Printf("write your text here  : ")
	var txt string
	fmt.Scanln(&txt)

	//take "txt" as input from user and write in the existing file
	_, err = file.WriteString(txt)
	if isError(err) {
		return
	}

	// Save file changes.
	err = file.Sync()
	if isError(err) {
		return
	}

	log.Println("File Updated Successfully.")
}

//for reading the data of the file
func ReadFile(path string) {
	// Open file for reading.
	var file, err = os.OpenFile(path, os.O_RDWR, 0777)
	if isError(err) {
		return
	}
	defer file.Close()

	// Read file, line by line
	var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)

		// Break if finally arrived at end of file
		if err == io.EOF {
			break
		}

		// Break if error occured
		if err != nil && err != io.EOF {
			isError(err)
			break
		}
	}

	fmt.Println("Reading from file.")
	fmt.Println(string(text))
}

//for handeling the error
func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}
