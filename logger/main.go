package main

import (
	"fmt"
	"log"

	logger "gitlab.com/mobocder-Shubham/gotraining/logger"
)

func main() {
	//get the directory from user were he/her want to create folder
	fmt.Println("Enter the directory : ")
	var loc string
	fmt.Scanln(&loc)

	/* if the path which enterd with user is valid then the logger package(Exist Method) return true
	and authenticate for further file creation otherwise they return path not exist or path is invalid */
	msg, _ := logger.Exists(loc)

	if msg == true {

		//if entered path is valid the execute this statement
		//path is taken as the file name with extension those want to created
		fmt.Println("Enter the file neme with extension : ")
		var path string
		fmt.Scanln(&path)

		//for creating a file (any type of file)
		logger.CreateFile(path)

		//edit the file those creatred above
		logger.WriteFile(path)

		//read the data of file those above created and edited
		logger.ReadFile(path)
	} else {

		//if path is not found this meassage is arrived
		log.Println("path not exist")
	}

}
