package main

import "fmt"

//
func main() {
	//initialize a slice with variable
	a := []int{100, 200, 300, 100, 200, 400, 500, 55, 0}
	fmt.Println(a)

	// store the slice without any duplicate element in x
	x := Removeduplicacy(a)

	//print the initialise slice without duplicacy
	fmt.Println(x)
}

//
func Removeduplicacy(a []int) []int {

	y := map[int]bool{}

	//declear slice in x variable
	x := []int{}

	for v := range a {
		if y[a[v]] == true {
			// here we don't add duplicates
		} else {
			y[a[v]] = true
			//store or append the data into x
			x = append(x, a[v])
		}
	}
	return x
}
