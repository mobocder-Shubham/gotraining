package welcome

import "fmt"

func Name(name string) string {
	message := fmt.Sprintf("welcome, %v. ", name)
	return message
}
