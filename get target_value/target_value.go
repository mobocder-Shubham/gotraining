package main

import "fmt"

func main() {

	//take size of array from user
	fmt.Printf("Enter size of your array: ")
	var size int
	fmt.Scanln(&size)

	//initilise the variable arr
	var arr = make([]int, size)

	//iterate here to take a element from user
	for i := 0; i < size; i++ {
		fmt.Printf("Enter %dth element: ", i)
		fmt.Scanf("%d", &arr[i])
	}

	//
	fmt.Printf("Enter the desired output:  ")
	var target int
	fmt.Scanln(&target)

	fmt.Println("Your array is: ", arr)

	// iterate for adding element of array as desire targeted value
	//first itrate using for loop after check condition with if loop
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr); j++ {
			if arr[i]+arr[j] == target {
				print("[", i, ", ", j, "]\n")
			} else {
				fmt.Println("target is not valid")
			}

		}

	}
}
