package main

import (
	"fmt"
	"net/http"

	"gitlab.com/mobocder-Shubham/gotraining/pkg/handlers"
)

const portno = ":8095"

func main() {
	http.HandleFunc("/", handlers.Home)
	http.HandleFunc("/about", handlers.About)

	fmt.Println(fmt.Sprintf("starting application on %s", portno))

	http.ListenAndServe(portno, nil)
}

/* func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("now reading the home page")
	renderTemplate(w, "home.html")
}

func About(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "about.html")
	fmt.Println("now reading the about page")

}

func renderTemplate(w http.ResponseWriter, templ string) {
	temp1, _ := template.ParseFiles("./templates/" + templ)
	err := temp1.Execute(w, nil)
	if err != nil {
		fmt.Println("error passing template : ", err)
		return
	}
} */
