package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/mobocder-Shubham/gotraining/pkg/render"
)

//home templates
func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("now reading the home page")
	render.RenderTemplate(w, "home.html")
}

//About templetes thats
func About(w http.ResponseWriter, r *http.Request) {
	render.RenderTemplate(w, "about.html")
	fmt.Println("now reading the about page")

}
