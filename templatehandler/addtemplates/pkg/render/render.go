package render

import (
	"fmt"
	"html/template"
	"net/http"
)

//this function is for rendering the templates using Html templates

func RenderTemplate(w http.ResponseWriter, templ string) {
	temp1, _ := template.ParseFiles("./templates/" + templ)
	err := temp1.Execute(w, nil)
	if err != nil {
		fmt.Println("error passing template : ", err)
		return
	}
}
