package main

import (
	"encoding/csv" //read and write csv file
	"fmt"
	"log"
	"os"
)

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath) // for read access

	//check the error condition for checking the file existance
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err) // if any error exist then print this message
	}
	defer f.Close() // for closing the executing file at the end of the program

	csvReader := csv.NewReader(f)       //start resding the file
	records, err := csvReader.ReadAll() //reading complete here in to the csv file

	//if error exist during reading in csv file
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

// main function
func main() {

	records := readCsvFile("/home/mobcoder-116/Desktop/username.csv")

	//gives desired output
	fmt.Println(records)
}
