package main

import (
	"fmt"

	welcome "github.com/shubhamdmobcoder/gotraining/welcome" //import from welcome subdirectory
)

func main() {

	//Here welcome is a package
	message := welcome.Name("Abhishek")
	fmt.Println(message)
}
