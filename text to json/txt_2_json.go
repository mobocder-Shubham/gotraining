package main 

import (
	"encoading/json"
	"fmt"

)

type Book struct {
	title string 'json: "name"'
	author string 'json: "author"'
}

func main() {
	fmt.Println("H")

	book := Book{title: "programming with go", author: "chrish wen" }
	fmt.Printf("%+v\n", book)

	byteArray , err := json.Marshal(book)
	if err != nil{
		fmt.println(err)

	}
	fmt.println(string(byteArray))
}