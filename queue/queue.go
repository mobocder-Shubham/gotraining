package main

import "fmt"

func main() {
	//initialize the queue variable
	var queue []int

	// enqueue value in the queue (1,20,3)
	queue = enqueue(queue, 1)
	queue = enqueue(queue, 20)
	queue = enqueue(queue, 3)

	//print all the element Present in a queue
	fmt.Println(queue)

	//initiallize the dqueue variable
	var dqueue []int

	//dequeue the values from queue FIFO
	dqueue = dequeue(queue)
	fmt.Println("Queue", dqueue)

}

//new function for enquied the element
func enqueue(queue []int, x int) []int {
	queue = append(queue, x)
	fmt.Println("enquied : ", x)
	return queue
}

//new function to dequied the element
func dequeue(queue []int) []int {
	element := queue[0]
	fmt.Println("dequeue:", element)
	return queue[1:]

}
