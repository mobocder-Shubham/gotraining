package helpers

import (
	"math/rand"
	"time"
)

func Randonnumber(n int) int {

	//for genrating the random no (change in nano second)
	rand.Seed(time.Now().UnixNano())
	val := rand.Intn(n)
	return val
}
