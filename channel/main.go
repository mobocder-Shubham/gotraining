package main

import (
	"fmt"
	"path/helpers"
)

// constant variable num having
const num = 10

//here we create the callable function
func Calculate(intChan chan int) {

	// random no saved in the randno variable by Randonumber Packages
	randno := helpers.Randonnumber(num)

	//here we sending the randno by channle intChan
	intChan <- randno
}

func main() {

	// initilise the channel with make as intChain variable
	intChan := make(chan int)

	//close the channel at the end of the execotion
	defer close(intChan)

	//genrate go routine by adding go as Prefix of the
	//callable function Calculate
	go Calculate(intChan)

	//here we recieve the data of randno by channel in num
	num := <-intChan

	//printed the recived data
	fmt.Println(num)
}
