package main

import (
	"encoding/json"
	"fmt"
)

type Person struct {
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	HairColour string `json:"hair_colour"`
	HasDog     bool   `json:"has_dog"`
}

func main() {

	myJson := `
[
	{
		"first_name": "Clark",
		"last_name": "Kent",
		"hair_colour": "black",
		"has_dog": true
	},
	{
		"first_name": "Peter",
		"last_name": "wen",
		"hair_colour": "brown",
		"has_dog": false
	}
]`

	//read json from struct
	var unmarshalled []Person

	err := json.Unmarshal([]byte(myJson), &unmarshalled)
	if err != nil {
		fmt.Println("Error un marshaled json", err)
	}

	//fmt.Println(unmarshalled)

	fmt.Printf("unmarshalled : %v \n", unmarshalled)

	// write json from struct

	var mySlice []Person

	var m1 Person
	m1.FirstName = "abhishek"
	m1.LastName = "singh"
	m1.HairColour = "white & black"
	m1.HasDog = false

	mySlice = append(mySlice, m1)

	var m2 Person
	m2.FirstName = "abhishek"
	m2.LastName = "singh"
	m2.HairColour = "white & black"
	m2.HasDog = false

	mySlice = append(mySlice, m2)

	newJson, err := json.MarshalIndent(mySlice, "", "        ")
	if err != nil {
		fmt.Println("err marshling ", err)
	}

	fmt.Println(string(newJson))
}
